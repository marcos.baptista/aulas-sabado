// @ts-check
// Array.filter Crie uma lista de textos e filtre os que tem mais de 10 caracteres Array.filter
var listaTextos = [];
listaTextos.push( "Texão extremamente gigantesco" );
listaTextos.push( "Filtre me" );
listaTextos.push( "Palavrões" );
listaTextos.push( "Paralelepípedo" );
listaTextos.push( "Inconstitucionalissimamente" );
listaTextos.push( "Palavrinhas" );
function filtreLista(listaTextos) {
    return listaTextos.length > 10
};
var filtro = listaTextos.filter(filtreLista);
console.log(filtro);

// Array.map - Crie uma lista de textos e reverta o conteudo deles
function reverteConteudo(listaTextos) {
    return listaTextos.split("").reverse().join("");
};
var listaReversa = listaTextos.map(reverteConteudo);
console.log(listaReversa);

// Array.sort - Crie uma lista de textos e ordene de ordem alfabética
var listaOrdenada = listaTextos.sort();
console.log(listaOrdenada);

// Array.reduce - Crie uma lista de numeros e some o valor de todos eles
var listaNumeros = [];
listaNumeros.push( 27 );
listaNumeros.push( 6 );
listaNumeros.push( 9 );
listaNumeros.push( 3 );
listaNumeros.push( 12 );
listaNumeros.push( 40 );
var listaSomada = listaNumeros.reduce(function(a,b) {
    return a+b; 
});
 console.log(listaSomada);