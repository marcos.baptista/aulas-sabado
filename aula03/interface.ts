interface Computador {
    ligar : () => void
    desligar : () => void
    softwares: string[]
    instalar: (sistemaOperacional:string) => void
    ip: string
}

class Gamer implements Computador {
    ligado = false
    ligar () {
        if ( this.ligado ) {
            throw "Computador já está ligado"
        }
        this.ligado = true;
        console.log("Computador ligado")
    }
    desligar () {
        if ( this.ligado ) {
            this.ligado = false;
            console.log("Seu computador foi desliado")
        }
    }
    softwares:string[] = []
    instalar (sistemaOperacional:string) {
        this.softwares.push(sistemaOperacional)
    }
    ip = ""
}

class Laptop implements Computador {
    ligado = false
    bateria = false
    ligar() {
        if ( !this.ligado ) {
            console.log("Computador ligado")
            this.ligado = true
            return;
        }
        if ( !this.bateria ) {
            console.log("Bateria carregada")
            this.bateria = true;
            return;
        }
        throw "Recarregar a bateria"
    }
    desligar () {
        if ( this.ligado ) {
            this.ligado = false;
            console.log("Seu computador foi desliado")
        }
    }
    softwares:string[] = []
    instalar (sistemaOperacional:string) {
        this.softwares.push(sistemaOperacional)
    }
    ip = ""
}

var computador: Computador = new Gamer()
//computador.ligar()
//computador.desligar()
computador.instalar("Windows")
computador.instalar("Urban Terror 4.03.2")
computador.ip = "10.10.1.33"

var laptop: Computador = new Laptop()
//laptop.ligar()
//laptop.desligar()
laptop.instalar("Linux")
laptop.instalar("Visual Studio Code")
computador.ip = "10.10.1.202"

//console.log(computador.softwares)
//console.log(laptop.softwares)

var computadorDavid: Computador = new Gamer()
computadorDavid.instalar("Linux")
computadorDavid.instalar("Eclipse")
computadorDavid.instalar("Visual Studio")
computadorDavid.ip = "10.10.1.191"

var computadorSouza: Computador = new Gamer()
computadorSouza.instalar("Linux")
computadorSouza.ip = "10.10.1.29"

var computadorRafael: Computador = new Laptop()
computadorRafael.instalar("Windows")
computadorRafael.instalar("Eclipse")
computadorRafael.ip = "10.10.1.18"

var listaDSV: Computador[] = []
listaDSV.push(computadorDavid)
listaDSV.push(computadorSouza)
listaDSV.push(computadorRafael)

console.log(listaDSV)