let num1: number = 9;
let num2: number = 3;
// Crie uma funcao para multiplicar dados
function multiplicar( dado1, dado2 ):number {
    return dado1 * dado2;
};
let totalMultiplicado: number = multiplicar ( num1, num2 );
console.log(totalMultiplicado);

// Crie uma funcao para subtrair dados
function subtrair( dado1, dado2 ):number {
    return dado1 - dado2;
};
let totalSubtraido: number = subtrair( num1, num2 );
console.log(totalSubtraido);

// Crie uma funcao para reverter uma string
function reverterString( string: string ): string {
    return string.split("").reverse().join("");
};
let stringReversa: string = reverterString("socorram me subi no onibus em marrocos");
console.log(stringReversa);

// Crie uma funcao para remover as vogais de uma string
function removeVogais( string: string ): string {
    return string.replace(/[aeiouà-ý]/gi,"");
};
let semVogais: string = removeVogais("Remove até as VOGAIS MAIÚSCULAS estando ou não acentuadas e palavras em alemão como Frankfürt ou Frölich")
console.log(semVogais);