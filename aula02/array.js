// Array.filter Crie uma lista de textos e filtre os que tem mais de 10 caracteres Array.filter
var listaTextos = ["Texão extremamente gigantesco", "Filtre me", "Palavrões", "Paralelepípedo", "Inconstitucionalissimamente", "Palavrinhas"];
function filtreLista(listaTextos) {
    return listaTextos.length > 10;
}
;
var filtro = listaTextos.filter(filtreLista);
console.log(filtro);
// Array.map - Crie uma lista de textos e reverta o conteudo deles
function reverteConteudo(listaTextos) {
    return listaTextos.split("").reverse().join("");
}
;
var listaReversa = listaTextos.map(reverteConteudo);
console.log(listaReversa);
// Array.sort - Crie uma lista de textos e ordene de ordem alfabética
var listaOrdenada = listaTextos.sort();
console.log(listaOrdenada);
// Array.reduce - Crie uma lista de numeros e some o valor de todos eles
var listaNumeros = [27, 6, 9, 3, 12, 40];
var listaSomada = listaNumeros.reduce(function (a, b) {
    return a + b;
});
console.log(listaSomada);
