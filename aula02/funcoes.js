var num1 = 9;
var num2 = 3;
// Crie uma funcao para multiplicar dados
function multiplicar(dado1, dado2) {
    return dado1 * dado2;
}
;
var totalMultiplicado = multiplicar(num1, num2);
console.log(totalMultiplicado);
// Crie uma funcao para subtrair dados
function subtrair(dado1, dado2) {
    return dado1 - dado2;
}
;
var totalSubtraido = subtrair(num1, num2);
console.log(totalSubtraido);
// Crie uma funcao para reverter uma string
function reverterString(string) {
    return string.split("").reverse().join("");
}
;
var stringReversa = reverterString("socorram me subi no onibus em marrocos");
console.log(stringReversa);
// Crie uma funcao para remover as vogais de uma string
function removeVogais(string) {
    return string.replace(/[aeiouà-ý]/gi, "");
}
;
var semVogais = removeVogais("Remove até as VOGAIS MAIÚSCULAS estando ou não acentuadas e palavras em alemão como Frankfürt ou Frölich");
console.log(semVogais);
