// Array.filter Crie uma lista de textos e filtre os que tem mais de 10 caracteres Array.filter
let listaTextos: Array<string> = ["Texão extremamente gigantesco","Filtre me","Palavrões","Paralelepípedo","Inconstitucionalissimamente","Palavrinhas"];

function filtreLista(listaTextos: string): boolean {
    return listaTextos.length > 10
};
let filtro: Array<string> = listaTextos.filter(filtreLista);
console.log(filtro);

// Array.map - Crie uma lista de textos e reverta o conteudo deles
function reverteConteudo(listaTextos: string): string {
    return listaTextos.split("").reverse().join("");
};

let listaReversa: Array<string> = listaTextos.map(reverteConteudo);
console.log(listaReversa);

// Array.sort - Crie uma lista de textos e ordene de ordem alfabética
let listaOrdenada: Array<string> = listaTextos.sort();
console.log(listaOrdenada);

// Array.reduce - Crie uma lista de numeros e some o valor de todos eles
let listaNumeros: Array<number> = [27,6,9,3,12,40]
let listaSomada: number = listaNumeros.reduce(function(a: number, b:number):number {
    return a+b; 
});
 console.log(listaSomada);
