//tipo de interface
var fun;
var exp;
function soma(param1, param2) {
    return param1 + param2;
}
;
function subtrai(param1, param2) {
    return param1 - param2;
}
;
function dividir(param1, param2) {
    return param1 / param2;
}
;
function multiplicar(param1, param2) {
    return param1 * param2;
}
;
function exponencial(base, expoente) {
    return Math.pow(base, expoente);
}
fun = soma;
fun = subtrai;
fun = dividir;
fun = multiplicar;
exp = exponencial;
console.log(soma(9, 3));
console.log(subtrai(9, 3));
console.log(dividir(9, 3));
console.log(multiplicar(9, 3));
console.log(exponencial(9, 2));
